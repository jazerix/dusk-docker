@extends('master')

@section('content')
    <div style="width: 800px;" class="container max-w-full mx-auto pt-4">
        <form action="{{ route('courses.create') }}" method="POST">
            @csrf
            <div class="bg-gray-200">
                <div class="container h-screen flex justify-center items-center">
                    <div class="relative">
                        <h2 class="text-xl font-bold text-gray-90">Create course</h2>
                        <div class="p-4 top-4 center-3">
                            <label for="name"> Course name:</label><br>
                            <input type="text"
                                   class="h-14 w-96 pl-10 pr-20 rounded-lg z-0 focus:shadow focus:outline-none"
                                   placeholder="Enter name..." name="name" id="name">
                        </div>
                        <div class="p-4 top-4 center-3">
                            <label for="name"> ECTS-value:</label><br>
                            <input type="text"
                                   class="h-14 w-96 pl-10 pr-20 rounded-lg z-0 focus:shadow focus:outline-none"
                                   placeholder="Enter ECTS..." name="ects" id="ects">
                        </div>
                        <div class="p-4 top-4 center-3">
                            <label for="code">Course code:</label><br>
                            <input type="text"
                                   class="h-14 w-96 pl-10 pr-20 rounded-lg z-0 focus:shadow focus:outline-none"
                                   placeholder="Enter code..." name="code" id="code">
                        </div>
                        <div class="p-4 top-4 center-3">
                            <label for="description"> Course description:</label><br>
                            <input type="text"
                                   class="h-14 w-96 pl-10 pr-20 rounded-lg z-0 focus:shadow focus:outline-none"
                                   placeholder="Enter description..." name="description" id="description">
                        </div>


                        <div class="p-4 top-4 center-3">
                            <label for="department_id">Department: </label>
                            <select style="background-color:white;"
                                    class="form-select h-14 w-96 block mt-1 rounded-lg pl-10 pr-20" name="department_id"
                                    id="department_id">
                                @foreach($departments as $department)
                                    <option value="{{ $department->id }}">{{ $department->name }}</option>
                                @endforeach
                            </select>
                            <input class="mt-4 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                                   type="submit"/>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
