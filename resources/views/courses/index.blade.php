@extends('master')

@section('content')
    <div style="width: 800px;" class="container max-w-full mx-auto pt-4">
        @if(session()->has('message'))
        <div class="alert alert-success success-message" role="alert">
            {{ session('message') }}
        </div>
        @endif
        <h2 class="text-xl font-bold text-gray-90">All courses</h2>
        <table class="table">
            <tr>
                <th style="text-align: left;">Name</th>
                <th>ECTS-value</th>
                <th>Code</th>
                <th>Description</th>
                <th>Department</th>
                <th>Actions</th>
            </tr>
            @foreach($courses as $course)
                <tr>
                    <td><a class="text-blue-500" href="{{ route('courses.show', $course->id) }}">{{ $course->name  }}</td>
                    <td>{{ $course->ects }}</td>
                    <td>{{ $course->code }}</td>
                    <td>{{ $course->description }}</td>
                    <td>{{ $course->department->name }} </td>
                    <td><a class="text-blue-500" href="{{ route('courses.edit', $course->id) }}">Edit</a></td>
                <tr>
            @endforeach
        </table>
        <a href="{{ route('courses.new') }}">
            <button class="mt-6 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" type="button">
                Create New
            </button>
        </a>
    </div>
@endsection
