@extends('master')

@section('content')
    <div style="width: 800px;" class="container max-w-full mx-auto pt-4">
        <div class=" bg-gray-200">
            <div class="container h-screen flex justify-center items-center">
                <div class="relative">
                    <h2 class="text-xl font-bold text-gray-90">Show course</h2>
                    <div class="p-2 top-2 center-2"><p class="h-14 w-96  pr-20 ">Name: {{ $course->name }}</p> </div>
                    <div class="p-2 top-2 center-2"><p class="h-14 w-96  pr-20 ">Code: {{ $course->code }}</p> </div>
                    <div class="p-2 top-2 center-2"><p class="h-14 w-96  pr-20 ">ECTS-value: {{ $course->ects }}</p> </div>
                    <div class="p-2 top-2 center-2"><p class="h-14 w-96  pr-20 ">Description: {{ $course->description }} </p>  </div>
                    <div class="p-2 top-2 center-2"><p class="h-14 w-96  pr-20 ">Department: {{ $course->department->name }} </p> </div>
                    <div class="p-2  top-4 center-3">
                        <a href="{{ route('courses.index') }}" class="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded">Go back</a>
                    </div>
                </div>

            </div>

        </div>
    </div>
@endsection
