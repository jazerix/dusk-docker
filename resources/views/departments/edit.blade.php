@extends('master')

@section('content')
    <div>
        <div class=" bg-gray-200">
            <div class="container flex justify-center items-center flex-column pt-4">
                <form  action="{{ route('departments.show', $department->id) }}" method="POST" class="relative">
                    @method('PUT')
                    @csrf
                    <h2 class="text-xl font-bold text-gray-90">Edit department</h2>
                    <div class="p-4 top-4 center-3"><i class="fa fa-search text-gray-400 z-20 hover:text-gray-500"></i>
                    </div>
                    <input type="text" class="h-14 w-96 pl-10 pr-20 rounded-lg z-0 focus:shadow focus:outline-none"
                           placeholder="Enter name..." name="name" value="{{ $department->name }}" id="name">
                    <div class="p-4 top-4 center-3"><i class="fa fa-search text-gray-400 z-20 hover:text-gray-500"></i>
                    </div>
                    <input type="text" class="h-14 w-96 pl-10 pr-20 rounded-lg z-0 focus:shadow focus:outline-none"
                           placeholder="Enter code..." name="code" value="{{ $department->code }}" id="code">
                    <div class="p-4 top-4 center-3"><i class="fa fa-search text-gray-400 z-20 hover:text-gray-500"></i>
                    </div>
                    <input type="text" class="h-14 w-96 pl-10 pr-20 rounded-lg z-0 focus:shadow focus:outline-none"
                           placeholder="Enter description..." name="description" value="{{ $department->description }}"
                           id="description">
                    <div class="top-4 center-3 mt-4">
                        <input class="submit bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                               type="submit" value="Save"/>
                        <a href="{{ route('departments.index') }}"
                           class="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded">Cancel</a>
                    </div>
                </form>

                <form action="{{ route('departments.destroy', $department->id) }}" method='post' class="w-100">
                    @csrf
                    @method('delete')
                    <button class="ml-4 bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded">Delete</button>
                </form>
            </div>
        </div>
    </div>

@endsection
