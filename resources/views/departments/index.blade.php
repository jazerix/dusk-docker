@extends('master')

@section('content')
    <div style="width: 800px;" class="container max-w-full mx-auto pt-4 bg-grey-200">
        <h2 class="text-xl font-bold text-gray-90">All departments</h2>
        @if(session()->has('message'))
            <div class="alert alert-success success-message" role="alert">
                {{ session('message') }}
            </div>
        @endif
        <table class="table">
            <tr>
                <th>Name</th>
                <th>Code</th>
                <th>Description</th>
                <th></th>
            </tr>
            @foreach($departments as $department)
                <tr class="department">
                    <td><a class="text-blue-500" href="/departments/{{ $department->id }}">{{ $department->name  }}</td>
                    <td>{{ $department->code }}</td>
                    <td style="width: 240px">{{ $department->description}}</td>
                    <td><a class="text-blue-500 edit" href="/departments/{{ $department->id }}/edit">Edit</a></td>
                <tr>
            @endforeach
        </table>

        <a href="{{ route('departments.new') }}"
           class="new mt-6 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
            Create New
        </a>
    </div>
@endsection
