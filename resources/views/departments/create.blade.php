@extends('master')

@section('content')
    <div class="container mx-auto pt-4">
        <form action="{{ route('departments.create') }}" method="POST">
            @csrf

            <div class=" bg-gray-200">
                <div class="py-8 flex justify-center items-center">
                    <div class="relative">
                        <h2 class="text-xl font-bold text-gray-90">Create department</h2>
                        <div class="p-4 top-4 center-3"><i
                                class="fa fa-search text-gray-400 z-20 hover:text-gray-500"></i></div>
                        <input type="text" class="h-14 w-96 pl-10 pr-20 rounded-lg z-0 shadow-sm focus:shadow focus:outline-none"
                               placeholder="Enter name..." name="name" id="name">
                        <div class="p-4 top-4 center-3"><i
                                class="fa fa-search text-gray-400 z-20 hover:text-gray-500"></i></div>
                        <input type="text" class="h-14 w-96 pl-10 pr-20 rounded-lg z-0  shadow-sm focus:shadow focus:outline-none"
                               placeholder="Enter code..." name="code" id="code">
                        <div class="p-4 top-4 center-3"><i
                                class="fa fa-search text-gray-400 z-20 hover:text-gray-500"></i></div>
                        <input type="text" class="h-14 w-96 pl-10 pr-20 rounded-lg z-0  shadow-sm focus:shadow focus:outline-none"
                               placeholder="Enter description..." name="description" id="description">
                        <div class="pt-8 top-4 center-3">
                            <input class="submit bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                                   type="submit" value="Create"/>


                            <a href="{{ route('departments.index') }}"
                               class="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded">Cancel</a>

                        </div>
                    </div>

                </div>

            </div>


        </form>
    </div>
@endsection
