@extends('master')

@section('content')
    <div class="container main">
        <p>Welcome to WebTech ItsLearning</p>
        <h3 class="display-3"> Main </h3>
        <a href="{{ route('departments.index') }}" class="departments-link">Departments</a><br>
        <a href="{{ route('courses.index') }}" class="courses-link">Courses</a>
        {{ \Illuminate\Support\Facades\DB::connection()->getDatabaseName() }}
    </div>
@endsection
