<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page;

class DepartmentPage extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/departments';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param Browser $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@code'        => 'input[name=code]',
            '@description' => 'input[name=description]',
            '@name'        => 'input[name=name]',
        ];
    }

    public function createNewDepartment(Browser $browser)
    {
        $browser
            ->assertPresent('.new');/*
            ->click(".new")
            ->assertPathIs("/departments/new")
            ->assertPresent('@code')
            ->assertPresent('@name')
            ->assertPresent('@description')
            ->type('name', 'Imada')
            ->type('description', 'Institute of Mathematics and Computer Science')
            ->type('code', 'XYZ-IMADA')
            ->press('.submit')
            ->assertPathIs('/departments')
            ->assertSee('Imada')
            ->assertSee('Institute of Mathematics and Computer Science')
            ->assertSee('XYZ-IMADA')
            ->assertSee('Department XYZ-IMADA created successfully');*/
    }

    public function editNewDepartment(Browser $browser)
    {
        $browser->assertSee('Imada')
            ->click('.edit')
            ->assertPathIs('/departments/*')
            ->assertPresent('@code')
            ->assertPresent('@name')
            ->assertPresent('@description')
            ->assertInputValue('code', 'XYZ-IMADA')
            ->assertInputValue('name', 'Imada')
            ->assertInputValue('description', 'Institute of Mathematics and Computer Science')
            ->type('code', 'ZYX-IMADA')
            ->type('name', 'MIMADA')
            ->type('description', 'Tek is probably better than Imada')
            ->press('.submit')
            ->assertPathIs("/departments/*")
            ->assertSee('Tek is probably better than Imada')
            ->assertSee('ZYX-IMADA')
            ->assertSee('MIMADA')
            ->assertSee('Department ZYX-IMADA updated successfully');
    }
}
