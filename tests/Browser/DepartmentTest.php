<?php

namespace Tests\Browser;

use App\Models\Department;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\DepartmentPage;
use Tests\DuskTestCase;

class DepartmentTest extends DuskTestCase
{
    use RefreshDatabase;

    /**
     *
     * @return void
     */
    public function testCreateNewDepartment()
    {
        $this->browse(function (Browser $browser)
        {
            $browser->visit("/departments")
                ->assertPresent('.new');
            /*$browser->visit(new DepartmentPage)
                ->createNewDepartment();*/
        });
    }

    /*public function testEditNewDepartment()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit(new DepartmentPage)
                ->editNewDepartment();
        });
    }*/
}
