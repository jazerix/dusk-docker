<?php

use App\Http\Controllers\CourseController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\MainController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [MainController::class, 'index'])->name('home');

Route::group(['prefix' => 'courses', 'as' => 'courses.'], function() {
    Route::get('/', [CourseController::class, 'index'])->name('index');
    Route::post('/', [CourseController::class, 'create'])->name('create');
    Route::get('new', [CourseController::class, 'new'])->name('new');
    Route::delete('{course}', [CourseController::class, 'destroy'])->name('destroy');
    Route::get('{course}', [CourseController::class, 'show'])->name('show');
    Route::put('{course}', [CourseController::class, 'update'])->name('update');
    Route::get('{course}/edit', [CourseController::class, 'edit'])->name('edit');
});


Route::group(['prefix' => 'departments', 'as' => 'departments.'], function() {
    Route::get('/', [DepartmentController::class, 'index'])->name('index');
    Route::post('/', [DepartmentController::class, 'create'])->name('create');
    Route::get('new', [DepartmentController::class, 'new'])->name('new');
    Route::get('{department}/edit', [DepartmentController::class, 'edit'])->name('edit');
    Route::get('{department}', [DepartmentController::class, 'show'])->name('show');
    Route::put('{department}', [DepartmentController::class, 'update'])->name('update');
    Route::delete('{department}', [DepartmentController::class, 'destroy'])->name('destroy');
});
