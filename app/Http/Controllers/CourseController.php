<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Department;


class CourseController extends Controller
{
    public function index()
    {

        $courses = Course::all();

        return view('courses.index', ['courses' => $courses]);
    }

    public function show(Course $course)
    {
        return view('courses.show', ['course' => $course]);
    }

    public function new()
    {
        $departments = Department::all();
        return view('courses.create', ['departments' => $departments]);
    }

    public function create()
    {
        request()->validate([
            'name'          => 'required',
            'ects'          => 'required',
            'code'          => 'required',
            'description'   => 'required',
            'department_id' => 'required'
        ]);

        $course = Course::create([
            'name'          => request('name'),
            'ects'          => request('ects'),
            'code'          => request('code'),
            'description'   => request('description'),
            'department_id' => request('department_id')
        ]);

        return redirect()->route('courses.index')->with('message', "Course $course->name created successfully");
    }

    public function edit(Course $course)
    {
        $departments = Department::all();

        return view('courses.edit', ['course' => $course, 'departments' => $departments]);

    }

    public function update(Course $course)
    {
        request()->validate([
            'name'          => 'required',
            'code'          => 'required',
            'description'   => 'required',
            'department_id' => 'required'
        ]);

        $course->update([
            'name'          => request('name'),
            'code'          => request('code'),
            'description'   => request('description'),
            'ects'          => request('ects'),
            'department_id' => request('department_id')
        ]);

        return redirect('/courses')->with('message', "Course $course->name updated successfully");
    }

    public function destroy(Course $course)
    {
        $course->delete();

        return redirect()->route('courses.index')->with('message', "Course $course->name deleted successfully");
    }
}
