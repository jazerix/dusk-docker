<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Course;
use Illuminate\Http\Request;


class DepartmentController extends Controller
{
    public function index()
    {

        $departments = Department::all();

        return view('departments.index', ['departments' => $departments]);
    }

    public function show(Department $department)
    {
        return view('departments.show', ['department' => $department]);
    }

    public function edit(Department $department)
    {

        return view('departments.edit', ['department' => $department]);

    }

    public function new()
    {

        return view('departments.create');
    }

    public function create()
    {
        request()->validate([
            'name'        => 'required',
            'code'        => 'required',
            'description' => 'required'
        ]);

        $department = Department::create([
            'name'        => request('name'),
            'code'        => request('code'),
            'description' => request('description')
        ]);

        return redirect()->route('departments.index')->with('message', "Department $department->code created successfully");
    }

    public function update(Department $department)
    {

        request()->validate([
            'name'        => 'required',
            'code'        => 'required',
            'description' => 'required'
        ]);

        $department->update([
            'name'        => request('name'),
            'code'        => request('code'),
            'description' => request('description')

        ]);

        return redirect()->route('departments.show', $department->id)->with('message', "Department $department->code updated successfully");
    }

    public function destroy(Department $department)
    {

        $department->delete();

        return redirect()->route('departments.index');
    }
}
